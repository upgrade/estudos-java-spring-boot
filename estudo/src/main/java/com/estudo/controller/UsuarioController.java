package com.estudo.controller;

import java.util.List;
import java.util.Optional;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.estudo.model.Usuario;
import com.estudo.repository.UsuarioRepository;

@RestController
public class UsuarioController {
	
	@Autowired
	private UsuarioRepository usuariorepository;
	
	@CrossOrigin(origins = "*")
	@GetMapping("/usuarios")
	public List<Usuario> buscarTodos(){
		return usuariorepository.findAll();
	}
	
	@CrossOrigin(origins = "*")
	@PostMapping("/usuarios")
	public void cadastrar(@RequestBody Usuario usuario) {
		usuariorepository.save(usuario);
	}
	
	@CrossOrigin(origins = "*")
	@GetMapping("/usuario/{id}")
	public Optional<Usuario> alterar(@PathVariable("id") Integer id) {
		return usuariorepository.findById(id);
	}
	
	@CrossOrigin(origins = "*")
	@PostMapping("/usuario/{id}")
	public void salvarUsuario(@PathVariable("id") Integer id, @RequestBody Usuario usuario) {
		usuariorepository.save(usuario);
	}
	
	@CrossOrigin(origins = "*")
	@DeleteMapping("/usuario/{id}")
	public void remover(@PathVariable("id") Integer id) {
		usuariorepository.deleteById(id);
	}
	
	

}
