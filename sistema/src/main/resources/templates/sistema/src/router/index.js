import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Clientes from '@/components/Clientes'
import CadastroClientes from '@/components/CadastroClientes'
import AlterarClientes from '@/components/AlterarClientes'
import ExcluirCliente from '@/components/ExcluirCliente'
import Produtos from '@/components/Produtos'
import CadastrarProdutos from '@/components/CadastrarProdutos'
import AlterarProduto from '@/components/AlterarProduto'
import ExcluirProduto from '@/components/ExcluirProduto'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/alterar/:id',
      name: 'AlterarClientes',
      component: AlterarClientes
    },
    ,
    {
      path: '/excluir/:id',
      name: 'ExcluirCliente',
      component: ExcluirCliente
    },
    
    {
      path: '/cadastrar',
      name: 'Cadastrar Clientes',
      component: CadastroClientes
    },
    {
      path: '/clientes',
      name: 'Clientes',
      component: Clientes
    },
    {
      path: '/produtos',
      name: 'Produtos',
      component: Produtos
    }
    ,
    {
      path: '/cadastrarProduto',
      name: 'Cadastrar Produtos',
      component: CadastrarProdutos
    },
    {
      path: '/alterarProduto/:id',
      name: 'Alterar Produtos',
      component: AlterarProduto
    }
    ,
    {
      path: '/excluirProduto/:id',
      name: 'Excluir Produtos',
      component: ExcluirProduto
    }
  ]
})
