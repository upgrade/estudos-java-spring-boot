package com.sistema.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sistema.model.Produto;

public interface ProdutoRepository extends JpaRepository<Produto, Integer> {

}
