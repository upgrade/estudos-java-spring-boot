package com.sistema.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sistema.model.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, Integer>{

}
