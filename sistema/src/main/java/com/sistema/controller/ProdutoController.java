package com.sistema.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.sistema.model.Produto;
import com.sistema.repository.ProdutoRepository;

@RestController
@CrossOrigin(origins = "*")
public class ProdutoController {

	@Autowired
	private ProdutoRepository prodRepository;
	
	
	@GetMapping("/produtos")
	public List<Produto> lista(){
		return prodRepository.findAll();
	}
	
	
	@PostMapping("/salvarproduto")
	public void salvar(@RequestBody Produto produto) {
		prodRepository.save(produto);
	}
	
	@GetMapping("/alterarProduto/{id}")
	public Optional<Produto> alterar(@PathVariable("id") Integer id){
		return prodRepository.findById(id);
	}
	
	@PostMapping("/salvarAlteracao/{id}")
	public void salvarAlteracao(@PathVariable("id") Integer id, @RequestBody Produto produto) {
		prodRepository.save(produto);
	}
	
	@PostMapping("/deletarProduto/{id}")
	public void deletar(@PathVariable("id") Integer id) {
		prodRepository.deleteById(id);
	}
	
}
