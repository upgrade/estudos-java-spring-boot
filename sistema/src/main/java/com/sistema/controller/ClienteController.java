package com.sistema.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.sistema.model.Cliente;
import com.sistema.repository.ClienteRepository;

@RestController
@CrossOrigin(origins="*")
public class ClienteController {
	
	@Autowired
	private ClienteRepository clienteRep;
	
	@GetMapping("/clientes")
	public List<Cliente> lista(){
		return clienteRep.findAll();
	}
	
	@PostMapping("/clientes")
	public void cadastrar(@RequestBody Cliente cliente) {
		clienteRep.save(cliente);
	}
	
	@GetMapping("/cliente/{id}")
	public Optional<Cliente> alterar(@PathVariable("id") Integer id){
		return clienteRep.findById(id);
	}
	
	@PostMapping("/cliente/{id}")
	public void salvar(@PathVariable("id") Integer id, @RequestBody Cliente cliente) {
		clienteRep.save(cliente);
	}
	
	@PostMapping("/excluircliente/{id}")
	public void deletar(@PathVariable("id") Integer id) {
		clienteRep.deleteById(id);
	}
	
	
	
	
}
